$(document).ready(function() {
  /*Code to activate the navigation bar*/
  $('.drawer').drawer();

  /*-----------------------------------------------------------*/

  /*Code to make the cart logo brighter*/
  var clicked = false;

  $("#sales").find('a').click(function(){
    clicked = true;
    $(this).css('opacity', '1');
  });

    /*-----------------------------------------------------------*/

  /*Code that gives price of trip in Vacation Planner depending on what they choose*/
  $("#vacationPriceButton").click(function() {
    var from = $("#form1Selection option:selected").text();
    var to = $("#form2Selection option:selected").text();
    var val1 = parseInt($("#form1Selection option:selected").val());
    var val2 = parseInt($("#form2Selection option:selected").val());

    $("#fromLocation").text("From: " + from);
    $("#toLocation").text("To: " + to);
    $("#tripPrice").text("Trip Price: $" + (val1 + val2));
  })

    /*-----------------------------------------------------------*/

    /*Code for the simpleCart.js library -> Uses these payment details*/
    simpleCart({
      checkout: {
        type: "PayPal",
        email: "thisIsNotAReal45756ufgjftjr5urtur5yrtyr5y@Account.com",
        currency: "CAD"
      }
    });

});

  /*-----------------------------------------------------------*/

/*Code to alert the customer that their product was added to the cart*/
function addToCartAlert(){
  alert("The trip was added to your cart!");
}


/*Google Analytics API*/
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-104223719-1', 'auto');
ga('send', 'pageview');
